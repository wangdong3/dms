module.exports = {
    title: '文档管理系统', 
    description: '文档管理',
    head: [],
    base: '/docs',
    host: '172.23.21.162',
    port: 8085,
    dest: './docs/.vuepress/dist',
    themeConfig:{
	nav:[
	   {text: 'Home', link: '/'},
	   {text: 'Share', link: '/share/vuepress搭建静态网站'},
	   {text: 'CSDN', link: 'http://blog.csdn.net'},
	   {text: 'Github', link: 'https://github.com'}
	],
     sidebar: {
		  '/': [
	       	        {
			  title:'vuepress相关',
			  collapsable:true,
			  children:[
				'/share/vuepress搭建静态网站'
			  ]
		        }
		  ]
     },
     sidebarDepth: 1,
     serviceWorker: true
    }
}
